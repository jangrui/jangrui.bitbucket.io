# 三十三. 涂刷隧道标志作业

![图片34.png](https://wiki-img.jangrui.com/image/gwwk/qsgzyzds/图片34.png "图片34.png")

## 1. 作业目的

使隧道内标志清晰，美化隧道外观；方便作业人员对隧道位置、里程的识别。

## 2. 作业准备

### 2.1 工作量调查

作业前进行工作量调查

### 2.2 主要工（机）具、材料准备

- **2.2.1** **工具：** 大刷子、刷浆桶、钢丝刷、铁铲等。

- **2.2.2** **材料：** 白灰、食盐、印板等。

- **2.2.3** 上道前检查工（机）具的安全性能，禁止工（机）具等带病上道作业，作业前后进行确认，以防止侵限或遗留在线路上。

### 2.3 基本作业人员

作业负责人1人，除垢1人，粉刷1人。

### 2.4 作业布置

按照维修作业一次标准化流程，提前做好工作量调查，编制作业方案，并进行分工布置。

## 3. 作业流程及标准

### 3.1清扫灰垢。

彻底清除掉旧有灰垢。

### 3.2粉刷。

涂刷均匀，字迹清楚，正确。

### 3.3检查

按照作业质量标准对隧道标志涂刷质量进行检查，发现不符合要求及时整改，确保作业质量达标。

### 3.4作业结束

清扫作业现场及回收清理料具。

### 3.5 完工会

收工后，施工（作业）负责人组织召开完工会，对当日作业质量及安全情况进行总结。

## 4. 主要质量标准

标志位置、式样符合要求，尺寸字样准确，标志清晰。

## 5. 记录及表格

- **5.1** 《维修作业方案制定表》、《维修作业方案布置表》、《维修作业完工表》。
- **5.2** 《桥隧检查记录簿》。
- **5.3** 《桥隧保养/维修日计划及完成表》。
- **5.4** 与本项作业有关的其他记录与表格等。

## 6. 风险名称：

>人身伤害

### 6.1 防控措施：

- **6.1.1** 列车到来前及时待避，所有料具不侵限。
- **6.1.2** 调灰浆时要戴胶手套。
- **6.1.3** 携带照明工具，作业前选好待避洞。