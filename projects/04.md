<h1>四. 单根抽换桥枕作业</h1>

<p style="text-align:center"><img alt="图片5.png" src="https://wiki-img.jangrui.com/image/gwwk/qsgzyzds/%E5%9B%BE%E7%89%875.png" /></p>

<h2>1. 作业目的</h2>

<p style="text-indent: 2em">更换桥上失效桥枕，保证桥上线路几何尺寸符合规定，确保行车安全</p>

<h2>2. 作业准备</h2>

<h3 style="padding:0 1em">2.1 工作量调查</h3>

<p style="text-indent: 2em">作业前进行工作量调查。</p>

<h3 style="padding:0 1em">2.2 主要工（机）具、材料准备</h3>

<table>
	<thead>
		<tr>
			<th style="text-align:center;">2.2.1</th>
			<th style="text-align:center;">工具：</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align:center;">撬棍</td>
			<td style="text-align:center;">枕木夹剪</td>
			<td style="text-align:center;">起钉器</td>
			<td style="text-align:center;">道钉锤（或活口扳手）</td>
			<td style="text-align:center;">扭力扳手</td>
			<td style="text-align:center;">电动扳手</td>
		</tr>
		<tr>
			<td style="text-align:center;">电钻</td>
			<td style="text-align:center;">手锯</td>
			<td style="text-align:center;">木钻</td>
			<td style="text-align:center;">锛子等木工工具</td>
			<td style="text-align:center;">钢梁除锈工具</td>
			<td style="text-align:center;">水平道尺</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th style="text-align:center;">2.2.2</th>
			<th style="text-align:center;">材料：</th>
			<th style="text-align:center;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align:center;">防腐油</td>
			<td style="text-align:center;">氟化钠</td>
			<td style="text-align:center;">木楔</td>
		</tr>
		<tr>
			<td style="text-align:center;">木塞</td>
			<td style="text-align:center;">桥枕或新桥枕</td>
			<td style="text-align:center;">等</td>
		</tr>
	</tbody>
</table>

<blockquote>
<p style="text-indent: 2em"><strong>注：</strong> 规格尺寸符合要求。</p>
</blockquote>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>2.2.3</strong> 上道前检查工（机）具的安全性能，禁止工（机）具等带病上道作业，作业前后进行确认，以防止侵限或遗留在线路上。</li>
</ul>

<h3 style="padding:0 1em">2.3 基本作业人员</h3>

<p style="text-indent: 2em">作业负责人1人，基本作业4-6人一组。</p>

<h3 style="padding:0 1em">2.4 作业布置</h3>

<p style="text-indent: 2em">按照维修作业一次标准化流程，提前做好工作量调查，编制作业方案，并进行分工布置。</p>

<h2>3. 作业流程及标准</h2>

<h3 style="padding:0 1em">3.1 准备作业</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>3.1.1</strong> 制作再用桥枕或新桥枕，运入桥孔附近的人行道上，非封锁时间不得在线路上搬运，无人行道时放在桥头。</li>
	<li style="padding:0 1em"><strong>3.1.2</strong> 拆除中心步板；钉尖向下、放牢。</li>
	<li style="padding:0 1em"><strong>3.1.3</strong> 卸钩螺栓：每隔一根桥枕卸去一根桥枕的钩螺栓，卸下后及时带帽，放在固定地点放牢。</li>
	<li style="padding:0 1em"><strong>3.1.4</strong> 起道钉或卸开扣件：每个枕头保留两个道钉（螺纹道钉），起掉其余道钉。</li>
	<li style="padding:0 1em"><strong>3.1.5</strong> 上盖板去污、除锈应事先处理。</li>
</ul>

<h3 style="padding:0 1em">3.2 基本作业：</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>3.2.1</strong> &ldquo;天窗&rdquo;开始手续办妥，拆除护木，抬起基本轨、护轨，垫好楔木；抬起高度以能抽出桥枕为宜。</li>
	<li style="padding:0 1em"><strong>3.2.2</strong> 抽出旧桥枕：撤除铁垫板，抽出旧桥枕放在人行道上另一侧。</li>
	<li style="padding:0 1em"><strong>3.2.3</strong> 穿入新桥枕：定位后铺设防磨垫板，铁垫板。</li>
	<li style="padding:0 1em"><strong>3.2.4</strong> 落轨打钉：桥枕钻钉孔防腐处理后，打齐道钉或拧紧螺纹道钉。</li>
</ul>

<h3 style="padding:0 1em">3.3 作业结束</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>3.3.1</strong> 安装护木：高低左右平顺、搭接严实。钩螺栓安装齐全。</li>
	<li style="padding:0 1em"><strong>3.3.2</strong> 上螺栓：涂油均匀，拧紧（分开式扣件应用扭力扳手复核扭矩）。</li>
	<li style="padding:0 1em"><strong>3.3.3</strong> 安装步行板，运出旧桥枕。</li>
</ul>

<h3 style="padding:0 1em">3.4 销点</h3>

<p style="text-indent: 2em">设备经验收达到放行列车条件后，清扫作业现场及回收清理料具、撤除防护，按要求销点，开通线路。</p>

<h3 style="padding:0 1em">3.5 过车后作业回检</h3>

<p style="text-indent: 2em">线路开通首列后，由作业负责人全面检查质量，确保轨道几何尺寸满足开通条件。达到作业验收标准后方可收工，否则按规定组织返工，直至合格。</p>

<h3 style="padding:0 1em">3.6 完工会</h3>

<p style="text-indent: 2em">收工后，施工（作业）负责人组织召开完工会，对当日作业质量及安全情况进行总结。</p>

<h2>4. 主要质量标准</h2>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>4.1</strong> 桥枕规格及材质符合标准。</li>
	<li style="padding:0 1em"><strong>4.2</strong> 桥枕与铁垫板之间铺设防磨胶垫，垫厚4～12mm，只铺1层，并无2mm及以上吊板。</li>
	<li style="padding:0 1em"><strong>4.3</strong> 桥枕槽口与钢梁盖板宽窄适度，最大缝隙不超过4mm。</li>
	<li style="padding:0 1em"><strong>4.4</strong> 桥枕底与钢梁联结系杆件（含钉栓）间隙大于3mm，与横梁间隙大于15mm。</li>
	<li style="padding:0 1em"><strong>4.5</strong> 钩、护螺栓顺直，中心左右偏差不超过4mm。</li>
	<li style="padding:0 1em"><strong>4.6</strong> 槽口、钉孔、栓孔、裂纹须经防腐灌缝处理。</li>
	<li style="padding:0 1em"><strong>4.7</strong> 桥枕要捆头，端头整齐成线。</li>
	<li style="padding:0 1em"><strong>4.8</strong> 护木与新抽换的桥枕扣紧密贴无2mm以上缝隙。</li>
</ul>

<h2>5. 记录及表格</h2>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>5.1</strong> 《维修作业方案制定表》、《维修作业方案布置表》、《维修作业完工表》。</li>
	<li style="padding:0 1em"><strong>5.2</strong> 《桥隧检查记录簿》。</li>
	<li style="padding:0 1em"><strong>5.3</strong> 《桥隧保养/维修日计划及完成表》。</li>
	<li style="padding:0 1em"><strong>5.4</strong> 与本项作业有关的其他记录与表格等。</li>
</ul>

<h2>6. 安全风险</h2>

<blockquote>
<p style="text-indent: 2em">人身伤害</p>
</blockquote>

<h3 style="padding:0 1em">6.1 防控措施：</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>6.1.1</strong> 确认封锁后，方可上道作业。</li>
	<li style="padding:0 1em"><strong>6.1.2</strong> 在无人行道桥上作业应安设临时作业架。禁止在钢轨外侧起钉；卸上螺栓时不得向桥外方向使劲，防止摔下。</li>
	<li style="padding:0 1em"><strong>6.1.3</strong> 抬起钢轨后不得将手脚伸入轨底；抽送桥枕要戴手套；使用枕木夹人要站稳，动作协调。禁止横跨站在被换桥枕头上。4.撤下的步行板要钉尖向下，放在人行道上，防止扎人。</li>
</ul>
