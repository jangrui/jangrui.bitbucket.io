# 二十九. 圬工梁横向并置加固作业

![图片30.png](https://wiki-img.jangrui.com/image/gwwk/qsgzyzds/图片30.png "图片30.png")

## 1. 作业目的

通过对梁体进行并置加固，增加梁体的横向刚度，保证行车安全。

## 2. 作业准备

### 2.1 工作量调查

作业前进行工作量调查

### 2.2 主要工（机）具、材料准备

- **2.2.1** 工具：钢筋探测仪、取芯钻、张拉顶、操纵台、电焊机、搅拌机、发电机。

- **2.2.2** 材料：钢管、吊钩、钢管扣件、竹跳板、铁丝、钢绞线、钢筋、模板、水泥、砂、碎石

- **2.2.3** 上道前检查工（机）具的安全性能，禁止工（机）具等带病上道作业，作业前后进行确认，以防止侵限或遗留在线路上。

### 2.3 基本作业人员

作业负责人1人，基本作业5-8人。

### 2.4 作业布置

按照维修作业一次标准化流程，提前做好工作量调查，编制作业方案，并进行分工布置。

## 3. 作业流程及标准

### 3.1 搭设脚手架。

- **3.1.1** 施工采用悬挂式脚手架，悬挂式脚手架采用φ16圆钢制作的吊钩、φ50钢管、竹跳板、钢管扣件、铁丝、防护网等材料。           

- **3.1.2** 脚手施工工序：
由桥墩台梁端向梁中悬挂吊钩----在同排的吊环内穿入纵向（平行于线路方向）钢管----纵向钢管穿入完成后，再在其上安装横向钢管并在其上满铺竹跳板----将纵、横向钢管及脚手架安装（绑扎）牢固并经检查合格后，施工作业人员系好安全带，在已成型的工作平台上继续进行吊环钩挂作业。依照此方法循环推进，直至桥下作业平台搭设成型。

- **3.1.3** 脚手平台加固措施：

	- **3.1.3.1** 在桥中线位置加设一排纵向钢管，纵、横向钢管间利用钢管扣件扣实（稳定横向钢管，限制吊环防松脱）。
	- **3.1.3.2** 为保证平台横向刚度，在隔板处利用方木、吊环增设中间吊点。
	- **3.1.3.3** 竹跳板用铁丝与横向钢管固定，梁体侧面与平台用方木支撑稳定，防止晃动。
	- **3.1.3.4** 平台两侧设置1.2m高的双层安全栏杆及防护网。

### 3.2 横向预应力筋孔位确认、放样。

- **3.2.1** 在新增横隔板处，用钢筋位置探测仪探明主梁腹板内钢筋的实际分布状态，再确定横向预应力筋穿越腹板的位置，避免钻伤梁主筋。
- **3.2.2** 探测前，先将梁体表面处理平整，用砂纸打磨或用水泥砂浆抹平，以保证探测结果精确。
- **3.2.3** 探测出梁内实际钢筋位置后，在梁体表面作出醒目的标记。
- **3.2.4** 钻孔位置与梁体钢筋有冲突时，可适当移动钻孔位置。

### 3.3 横向预应力筋孔位钻孔。

钻孔前，电锤钻杆必须保持水平，垂直腹板，然后接通电源，即可开钻。钻孔时，使钻杆保持平稳，钻杆转速均匀，以保证钻孔质量和钻机不被损坏。

### 3.4 钻锚固钢筋孔眼、梁体结合面凿毛。

- **3.4.1** 按图纸要求确定钢筋锚固孔眼位置，锚固φ16螺纹钢筋的钻孔直径为20mm，φ12螺纹钢筋的钻孔直径为φ16mm，锚固筋钻孔深度为10cm。
- **3.4.2** 成孔后，用清水冲洗孔眼，以保证锚固效果。
- **3.4.3** 将新增横隔板范围内的梁体表面凿毛并清洗干净，以增加新、旧混凝土之间的粘结。

### 3.5 钢筋锚固、绑扎隔板钢筋。

- **3.5.1** 锚垫板下因钻孔崩落的混凝土，须用环氧树脂砂浆填补平整。
- **3.5.2** 锚垫板在梁体变截面处时，须将锚垫板处混凝土凿成平面，保证锚垫板与锚具密贴。
- **3.5.3** 钢筋采用植筋胶锚固，按照植筋胶比例使用，植筋胶启用后必须一次性使用完成。
- **3.5.4** 钢筋按照24m、32m梁体横向加固布置图布置，将植筋胶填满孔眼后，立即插入螺纹钢筋，打紧，抹平孔眼。

### 3.6 穿预应力筋、压注黄油。

调整预应力筋的外露长度，安装锚具。钢绞线在下料前，应先准确测量两片梁腹板外侧间实际长度，以确定钢绞线的下料长度。
预应力筋穿设完成后，在预应力胶套管内压注黄油。

### 3.7 安装模板、横向预应力筋初张拉。

- **3.7.1** 横隔板的模板用竹胶板制作，并保证其有足够强度、刚度和稳定性。能可靠承受新灌注混凝土的重力及列车经过时所产生的振动荷载。
- **3.7.2** 模板表面平整，接合严密，并便于拆卸。
- **3.7.3** 模板与混凝土接触表面涂刷脱模剂。
- **3.7.4** 初张拉前，在两片梁体张拉位置处用枕木撑紧。
- **3.7.5** 预应力初张拉力按照24m、32m梁体横向加固设计图进行。

### 3.8 灌注隔板混凝土

- **3.8.1** 混凝土拌合物采用机械搅拌。
- **3.8.2** 混凝土集料采用重量法计量，其称量的允许误差：水泥为±2%，砂、石为±3%，水为±1%。
- **3.8.3** 灌注砼时，第一次灌入砼总量的1/2，随即用插入式振捣器振捣密实；第二次灌入砼直至与进料口平齐，再振捣密实；最后填实整个隔板并封口，封口后用铁锤分部振敲模板。
- **3.8.4** 在灌注混凝土同时，制作2组混凝土试块，与横隔板同等条件下养护，以提供终张拉时混凝土强度和28天龄期强度。

### 3.9 拆模、养护。

- **3.9.1**混凝土灌注完成后24小时即可拆除两端侧模。
- **3.9.2** 砼强度达到设计强度的80%方可拆承重模。
- **3.9.3** 若发现混凝土外观缺陷，立即用混凝土或砂浆修补。
- **3.9.4** 拆模后，混凝土先洒水进行养护。终张拉完成后，在砼表面涂刷一层养护剂。

### 3.10 横向预应力筋终张拉、端头封锚

- **3.10.1** 在混凝土强度达到设计强度的80%后，进行终张拉。预应力筋终张拉力为195KN。
- 3.10.2采用砂轮片切割外露预应力筋，切口距锚具3cm，严禁烧割。
- **3.10.3** 端头封锚：绑扎封锚钢筋→立模板→灌注封锚C50砼→拆模→养护。

### 3.11 拆除脚手架

### 3.12 检查

按照作业质量标准对圬工梁横向并置质量进行检查，发现不符合要求及时整改，确保作业质量达标。

### 3.13 作业结束

清扫作业现场及回收清理料具。

### 3.14 完工会

收工后，施工（作业）负责人组织召开完工会，对当日作业质量及安全情况进行总结。

## 4. 主要质量标准

- **4.1** 钢绞线规格等要符合技术条件要求。
- **4.2** 选用水泥时应通过试验决定。
- **4.3** 细骨料采用质硬洁净天然河沙，使用前筛除5mm以上石块及泥土杂物。粗骨料的粒径为5-25mm，针片状颗粒含量不得大于15%。
- **4.4** 预应力筋初张拉应力为0.15fpu(张拉力39KN)；在混凝土强度弹性模量达到设计值的80%后,方可进行预应力筋终张拉,张拉控制应力为0.75fpu(张拉力195KN)。
- **4.5** 预应力终张拉采用YQD230-100型千斤顶,先张拉到设计吨位,顶压锚固后,拉动换向阀重复张拉至设计吨位,拧紧承压螺母,以保证预应力筋回缩量小于1mm。
- **4.6** 混凝土强度达到设计强度的80%方可拆除模板。
- **4.7** 配套标定千斤顶的有郊期为1个月或200次张拉作业。

## 5. 记录及表格

- **5.1** 《施工作业方案制定表》、《施工作业方案布置表》、《施工作业完工表》。
- **5.2** 《桥隧检查记录簿》。
- **5.3** 《施工日计划及完成表》。
- **5.4** 与本项作业有关的其他记录与表格等。

## 6. 风险名称：

>高空坠落伤害

### 6.1 防控措施：

- **6.1.1** 作业人员在高于2m或以上的陡坡进行高空作业时，必须系好安全带或安全绳，
- **6.1.2** 高空作业、临边作业时必须设置防护围栏。
- **6.1.3** 醒目位置张贴安全警示标识。