<h1 style="text-align: center;">十二. 钢梁除锈作业</h1>

<p style="text-align:center"><img alt="图片13.png" src="https://wiki-img.jangrui.com/image/gwwk/qsgzyzds/%E5%9B%BE%E7%89%8713.png" /></p>

<h2>1. 作业目的</h2>

<p style="text-indent: 2em">对钢梁锈蚀进行处理，延长钢梁使用寿命。</p>

<h2>2. 作业准备</h2>

<h3 style="padding:0 1em">2.1 工作量调查</h3>

<p style="text-indent: 2em">作业前进行工作量调查</p>

<h3 style="padding:0 1em">2.2 主要工（机）具、材料准备</h3>

<h4>2.2.1 工具：</h4>

<table>
	<thead>
		<tr>
			<th style="text-align:center;">2.2.1.1</th>
			<th style="text-align:center;">喷砂除锈：</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align:center;">空压机（不小于3立方/min；风压不少于0.5MPa）</td>
			<td style="text-align:center;">贮风筒</td>
			<td style="text-align:center;">贮沙桶</td>
			<td style="text-align:center;">输风管</td>
		</tr>
		<tr>
			<td style="text-align:center;">油水分离器</td>
			<td style="text-align:center;">喷砂器</td>
			<td style="text-align:center;">筛砂机</td>
			<td style="text-align:center;">喷砂嘴及连接器</td>
		</tr>
		<tr>
			<td style="text-align:center;">运砂小车及装砂工具</td>
			<td style="text-align:center;">铁线</td>
			<td style="text-align:center;">活口扳手</td>
			<td style="text-align:center;">克丝钳子等工具</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th style="text-align:center;">2.2.1.2</th>
			<th style="text-align:center;">手工除锈：</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align:center;">铲刀</td>
			<td style="text-align:center;">敲锈锤</td>
			<td style="text-align:center;">钢丝刷</td>
			<td style="text-align:center;">手砂轮</td>
			<td style="text-align:center;">砂布等</td>
		</tr>
	</tbody>
</table>

<h4>2.2.2 材料：</h4>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>砂子：</code></strong> 洁净、干燥、无盐分、无沾污的石英砂或河砂，粒径0.5～2.0㎜。</li>
</ul>

<h4>2.2.3</h4>

<p style="text-indent: 2em">上道前检查工（机）具的安全性能，禁止工（机）具等带病上道作业，作业前后进行确认，以防止侵限或遗留在线路上。</p>

<h3 style="padding:0 1em">2.3 基本作业人员</h3>

<p style="text-indent: 2em">作业负责人1人，筛砂1～2人，运砂3～4人，装砂及操纵喷砂器1人，机械操作1人，喷砂：2～3人。</p>

<h3 style="padding:0 1em">2.4 作业布置</h3>

<p style="text-indent: 2em">按照维修作业一次标准化流程，提前做好工作量调查，编制作业方案，并进行分工布置。</p>

<h2>3. 作业流程及标准</h2>

<h3 style="padding:0 1em">3.1 搭设脚手架。</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>3.1.1</code></strong> 脚手架要搭设牢固。</li>
	<li style="padding:0 1em"><strong><code>3.1.2</code></strong> 不得侵入限界。</li>
	<li style="padding:0 1em"><strong><code>3.1.3</code></strong> 高空脚手架临空一侧应设置防护围栏和安全网。</li>
</ul>

<h3 style="padding:0 1em">3.2 喷砂准备工作。</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>3.2.1</code></strong> 准备好强度高的颗粒干砂，粒径0.5～2.0㎜。</li>
	<li style="padding:0 1em"><strong><code>3.2.2</code></strong> 检查喷砂机具和管路连接是否牢固。</li>
	<li style="padding:0 1em"><strong><code>3.2.3</code></strong> 开动空压机试风，风压保持0.4～0.65MPa。</li>
</ul>

<h3 style="padding:0 1em">3.3 喷砂除锈作业。</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>3.3.1</code></strong> 风压达到后先开喷砂筒风阀，再开砂阀进行喷砂除锈；结束时先关砂阀，后关风阀，防止堵塞。</li>
	<li style="padding:0 1em"><strong><code>3.3.2</code></strong> 喷砂嘴距钢梁表面间距为150～250㎜，喷射角45&deg;～80&deg;为宜，喷砂嘴为直径5～7㎜的高氧化铝陶瓷喷嘴。移动速度要均匀，除锈要彻底并不伤钢梁。</li>
	<li style="padding:0 1em"><strong><code>3.3.3</code></strong> 喷砂除锈顺序应由上到下，先喷死角等缝隙处,后喷大面宽敞处，先喷边缘处,后喷中间,先喷铆钉头后喷平面，逐步推进。</li>
	<li style="padding:0 1em"><strong><code>3.3.4</code></strong> 喷砂人员与控制喷砂人员要密切配合，加强联络，及时调整砂量。</li>
	<li style="padding:0 1em"><strong><code>3.3.5</code></strong> 喷砂完毕后，应用干燥清洁的压缩空气清除钢梁表面的砂粒，灰尘及锈沫等。</li>
	<li style="padding:0 1em"><strong><code>3.3.6</code></strong> 喷完后还应认真找细,清除残留物,并检查钢梁有无裂纹、伤损及不良铆钉。</li>
</ul>

<h3 style="padding:0 1em">3.4 检查</h3>

<p style="text-indent: 2em">按照作业质量标准对钢表面除锈进行检查，发现不符合要求及时整改，确保作业质量达标。</p>

<h3 style="padding:0 1em">3.5 作业结束</h3>

<p style="text-indent: 2em">清扫作业现场及回收清理料具。</p>

<h3 style="padding:0 1em">3.6 销点</h3>

<blockquote>
<p style="text-indent: 2em">（距接触网带电部分2m以内作业需封锁、停电）</p>
</blockquote>

<p style="text-indent: 2em">设备经验收达到放行列车条件后，清扫作业现场及回收清理料具、撤除防护，按要求销点，开通线路。</p>

<h3 style="padding:0 1em">3.7 完工会</h3>

<p style="text-indent: 2em">收工后，施工（作业）负责人组织召开完工会，对当日作业质量及安全情况进行总结。</p>

<h2>4. 主要质量标准</h2>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>5.1</code></strong> 钢梁表面无残留污泥、油垢、铁锈、旧漆皮和氧化皮。</li>
	<li style="padding:0 1em"><strong><code>5.2</code></strong> 钢梁表面洁净均匀，露出金属光泽。</li>
	<li style="padding:0 1em"><strong><code>5.3</code></strong> 钢梁表面无刀痕锤印。</li>
	<li style="padding:0 1em"><strong><code>5.4</code></strong> 电弧喷铝或涂装环氧富锌底漆时，钢梁表面清理应达到Sa3.0级。涂装酚醛红丹、醇酸红丹、聚氨脂底漆、维护涂装环氧富锌底漆或热喷锌时，钢梁表面清理应达到Sa2.5级。箱型梁内表面涂装环氧沥青底漆时，钢梁表面清理应达到Sa2.0级。</li>
	<li style="padding:0 1em"><strong><code>5.5</code></strong> 钢表面粗糙度为Rz25～60&mu;m；选用最大粗造度不得超过涂装体系干膜厚度的1/3。表面粗糙度超过要求时，需加涂一道底漆。电弧喷铝时，钢表面粗糙度为Rz25～100&mu;m；当表面粗糙度超过Rz100&mu;m时，涂层应至少超过轮廓峰125～150&mu;m。</li>
	<li style="padding:0 1em"><strong><code>5.6</code></strong> 钢梁连接板层之间大于0.5mm的缝隙须将缝内污垢和铁锈清除干净，在第一道底漆干燥后，用石膏腻子填塞，待腻子表面干燥后，方可继续进行涂料涂装。小于0.5mm的缝隙可用油漆封闭。腻子应与所用防锈底漆配套使用。</li>
</ul>

<h1>5. 记录及表格</h1>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>5.1</code></strong> 《维修作业方案制定表》、《维修作业方案布置表》、《维修作业完工表》。</li>
	<li style="padding:0 1em"><strong><code>5.2</code></strong> 《桥隧检查记录簿》。</li>
	<li style="padding:0 1em"><strong><code>5.3</code></strong> 《桥隧保养/维修日计划及完成表》。</li>
	<li style="padding:0 1em"><strong><code>5.4</code></strong> 与本项作业有关的其他记录与表格等。</li>
</ul>

<h2>6. 安全风险</h2>

<blockquote>
<p style="text-indent: 2em">人身伤害</p>
</blockquote>

<h3 style="padding:0 1em">6.1 防控措施：</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>6.1.1</code></strong> 距接触网带电部份不足2m，确认线路封锁停电后方可作业。</li>
	<li style="padding:0 1em"><strong><code>6.1.2</code></strong> 高空作业应系安全带或设置安全网，作业前应检查脚手架是否稳固</li>
	<li style="padding:0 1em"><strong><code>6.1.3</code></strong> 喷砂人员要佩戴安全防护用品,喷砂衣应密封,衣内砂尘含量不能超过2mg/m3，防护头罩内应输送经过滤的新鲜空气。</li>
	<li style="padding:0 1em"><strong><code>6.1.4</code></strong> 开启风、砂阀应严格按照操作程序进行,喷砂嘴不能侵入限界；喷砂时喷嘴不准对车、船、行人；桥上作业时要设专人防护，来车时关闭风阀，停止作业。</li>
	<li style="padding:0 1em"><strong><code>6.1.5</code></strong> 喷砂胶管不能弯折卡压，防止爆管伤人；风、砂管路要经常检查，磨损超限者及时更换。</li>
</ul>

<blockquote>
<p style="text-indent: 2em">触电</p>
</blockquote>

<h3 style="padding:0 1em">6.2 防控措施：</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong><code>6.2.1</code></strong> 电气化区段距离带电体2m以内的作业必须接触网停电。</li>
	<li style="padding:0 1em"><strong><code>6.2.2</code></strong> 作业平台线路侧必须装设防护网，防止长大杆件侵入导电区</li>
</ul>
