<h1 style="text-align: center;">五. 整孔更换桥枕作业</h1>

<p style="text-align:center"><img alt="图片6.png" src="https://wiki-img.jangrui.com/image/gwwk/qsgzyzds/%E5%9B%BE%E7%89%876.png" /></p>

<h2>1. 作业目的</h2>

<p style="text-indent: 2em">更换整孔桥枕失效率达<code>25%</code>及以上；行车速度大于<code>120km/h</code>区段整孔失效率<code>20%</code>及以上；大于<code>160km/h</code>区段整孔失效率<code>15%</code>及以上桥枕，保证桥上线路几何尺寸符合规定，确保行车安全</p>

<h2>2. 作业准备</h2>

<h3 style="padding:0 1em">2.1 工作量调查</h3>

<p style="text-indent: 2em">作业前进行工作量调查。</p>

<h3 style="padding:0 1em">2.2 主要工（机）具、材料准备</h3>

<table>
	<thead>
		<tr>
			<th style="text-align:center;">2.2.1</th>
			<th style="text-align:center;">工具：</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align:center;">道尺</td>
			<td style="text-align:center;">撬棍</td>
			<td style="text-align:center;">道钉锤</td>
			<td style="text-align:center;">螺丝板子</td>
			<td style="text-align:center;">扭力扳手</td>
			<td style="text-align:center;">电动扳手</td>
		</tr>
		<tr>
			<td style="text-align:center;">电钻</td>
			<td style="text-align:center;">尺条起道机</td>
			<td style="text-align:center;">抬轨夹剪或带钩大绳</td>
			<td style="text-align:center;">枕木夹钳</td>
			<td style="text-align:center;">锯</td>
			<td style="text-align:center;">锛</td>
		</tr>
		<tr>
			<td style="text-align:center;">扫帚</td>
			<td style="text-align:center;">直钉器</td>
			<td style="text-align:center;">水平尺</td>
			<td style="text-align:center;">施工防护用品等</td>
			<td style="text-align:center;">&nbsp;</td>
			<td style="text-align:center;">&nbsp;</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th style="text-align:center;">2.2.2</th>
			<th style="text-align:center;">材料：</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
			<th style="text-align:center;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align:center;">桥枕</td>
			<td style="text-align:center;">护木</td>
			<td style="text-align:center;">分开式扣件</td>
			<td style="text-align:center;">直径22mm钩螺栓</td>
			<td style="text-align:center;">110mm&times;110mm&times;8mm铁垫圈</td>
		</tr>
		<tr>
			<td style="text-align:center;">6～10mm同类型胶垫圈</td>
			<td style="text-align:center;">4～12mm防磨垫板</td>
			<td style="text-align:center;">20或22mm护木螺栓</td>
			<td style="text-align:center;">20或22mm防爬角钢螺栓</td>
			<td style="text-align:center;">道钉</td>
		</tr>
		<tr>
			<td style="text-align:center;">护轨木垫板</td>
			<td style="text-align:center;">防腐油</td>
			<td style="text-align:center;">氟化钠</td>
			<td style="text-align:center;">木楔</td>
			<td style="text-align:center;">短枕头</td>
		</tr>
	</tbody>
</table>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>2.2.3</strong> 上道前检查工（机）具的安全性能，禁止工（机）具等带病上道作业，作业前后进行确认，以防止侵限或遗留在线路上。</li>
</ul>

<h3 style="padding:0 1em">2.3 基本作业人员</h3>

<p style="text-indent: 2em">作业负责人1人，10～12人作业。</p>

<h3 style="padding:0 1em">2.4 作业布置</h3>

<p style="text-indent: 2em">按照维修作业一次标准化流程，提前做好工作量调查，编制作业方案，并进行分工布置。</p>

<h2>3. 作业流程及标准</h2>

<h3 style="padding:0 1em">3.1 准备作业</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>3.1.1</strong> 检查线路偏心、轨向、水平、长平等，事先进行整治、调整。</li>
	<li style="padding:0 1em"><strong>3.1.2</strong> 把作好的桥枕按编号运入桥内放在附近人行道上或运到桥头(非封锁时间不得在线路上搬运)；堆放不得侵入限界，堆放重量不得超过人行道允许承载能力（明桥面人行道承载能力一般为<code>4.0KPa</code>，人行道竖向集中荷载<code>150kg</code>）。</li>
	<li style="padding:0 1em"><strong>3.1.3</strong> 拆除中间步板，钉尖向下或打倒，抬出桥外堆放。</li>
</ul>

<h3 style="padding:0 1em">3.2 基本作业</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.1</strong> <strong>卸钩螺栓：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">每隔一个桥枕卸去一个钩螺栓，及时带帽，妥善放置。</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.2</strong> <strong>起道钉：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">拧下螺纹道钉</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.3</strong> 拆除或抬高护轨。</p>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.4</strong> <strong>撤去护木：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">螺栓及时带帽，护木及螺栓放在人行道上或运出桥外妥善堆码。</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.5</strong> 拔（拧）掉全部道钉。</p>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.6</strong> 卸去鱼尾板，把基本轨抬出桥外或移到邻孔。无缝线路桥上用起道机将基本轨抬起并垫好木垫。</p>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.7</strong> <strong>上盖板清扫及去污：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">锈蚀时应在整孔更换桥枕前，进行上盖板防绣处理。</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.8</strong> 运进新桥枕，按号就位，并上好防爬角钢螺栓。</p>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.9</strong> 运进基本轨，连接接头，拨正方向（最好事先在桥枕上画好基本轨位置线，按线放置钢轨）；或用起道机撤除木垫落下基本轨。</p>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.10</strong> <strong>安设弹条扣件：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">方向、轨距扭矩符合标准。</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.2.11</strong> 每隔一根桥枕上好钩螺栓，与护木公用的钩螺栓可临时垫短方木拧紧。</p>
	</li>
</ul>

<h3 style="padding:0 1em">3.3 恢复作业：</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.3.1</strong> <strong>运进及铺设护轨：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">钉道钉事先钻孔，并经防腐处理，护轨与桥枕间缝隙应垫木垫板或落下护轨。</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.3.2</strong> <strong>安装护木：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">上紧全部螺栓，通过列车后再次拧紧螺栓，直到全部密贴无松动。</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.3.3</strong> <strong>安装步行板：</strong></p>

	<ul style="padding:0 1em">
		<li style="padding:0 1em">连接牢固、顺直、梁端断开。</li>
	</ul>
	</li>
	<li style="padding:0 1em">
	<p style="text-indent: 2em"><strong>3.3.4</strong> 护木、步行板防腐涂油：要均匀不流淌。</p>
	</li>
</ul>

<h3 style="padding:0 1em">3.4 销点</h3>

<p style="text-indent: 2em">设备经验收达到放行列车条件后，清扫作业现场及回收清理料具、撤除防护，按要求销点，开通线路。</p>

<h3 style="padding:0 1em">3.5 过车后作业回检</h3>

<p style="text-indent: 2em">线路开通首列后，由作业负责人全面检查桥枕更换质量，确保轨道几何尺寸满足开通条件。达到作业验收标准后方可收工，否则按规定组织返工，直至合格。</p>

<h3 style="padding:0 1em">3.6 完工会</h3>

<p style="text-indent: 2em">收工后，施工（作业）负责人组织召开完工会，对当日作业质量及安全情况进行总结。</p>

<h2>4. 主要质量标准</h2>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>4.1</strong> 轨距水平轨向等符合线路维修标准，消除有害接头（应符合《桥隧建筑物大修维修规则》第3.1.4条规定），桥头两端线路各<code>75m</code>范围内加强锁定。</li>
	<li style="padding:0 1em"><strong>4.2</strong> 桥枕净距：为<code>100～180mm</code>（横梁处除外），根据运营情况，专用线可放宽到<code>210mm</code>。</li>
	<li style="padding:0 1em"><strong>4.3</strong> 桥枕容许挖深<code>30mm</code>以内的槽口，或使用比标准断面稍厚的桥枕，盖板厚度不同应采用不同断面桥枕进行调整。</li>
	<li style="padding:0 1em"><strong>4.4</strong> 全桥防磨胶垫厚度尽量一致，为<code>4～12mm</code>，双层胶垫不超过3%，胶垫总厚度<code>12～15mm</code>不超过<code>3%</code>。吊板不超过<code>1%</code>。</li>
	<li style="padding:0 1em"><strong>4.5</strong> 桥枕与螺栓头（铆钉头）接触处挖槽。</li>
	<li style="padding:0 1em"><strong>4.6</strong> 桥枕与上平联结系间保持<code>3mm</code>以上空隙。</li>
	<li style="padding:0 1em"><strong>4.7</strong> 桥枕捆头，锯头整齐成线。</li>
	<li style="padding:0 1em"><strong>4.8</strong> 栓孔、钉孔、槽口、裂缝必须防腐处理。</li>
	<li style="padding:0 1em"><strong>4.9</strong> 护木各项质量指标符合更换护木质量标准。</li>
	<li style="padding:0 1em"><strong>4.10</strong> 桥枕不能铺设在横梁上，与横梁翼缘边应留出<code>15mm</code>及以上缝隙。</li>
	<li style="padding:0 1em"><strong>4.11</strong> 横梁两侧桥枕间净距在<code>300mm</code>以上，且桥枕顶面高出横梁顶面<code>50mm</code>以上时，应在横梁上垫短枕承托，短枕与护轨应联结牢固，且与基本轨底留出<code>5～10mm</code>空隙。</li>
	<li style="padding:0 1em"><strong>4.12</strong> 每根桥枕需用两根经防腐处理的直径<code>22mm</code>标准型钩螺栓与钢梁钩紧。螺栓均应配以<code>110mm&times;110mm&times;8mm</code>的铁垫圈及<code>10～20mm</code>厚的木垫圈或<code>6～10mm</code>胶垫圈。在自动闭塞区间，钩螺栓铁垫圈与钢轨扣件间应留不小于<code>15mm</code>的间隙，以防止轨道电路短路。</li>
	<li style="padding:0 1em"><strong>4.13</strong> 整孔更换新桥枕，应安设弹条扣件。</li>
</ul>

<h2>5. 记录及表格</h2>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>5.1</strong> 《维修作业方案制定表》、《维修作业方案布置表》、《维修作业完工表》。</li>
	<li style="padding:0 1em"><strong>5.2</strong> 《桥隧检查记录簿》。</li>
	<li style="padding:0 1em"><strong>5.3</strong> 《桥隧保养/维修日计划及完成表》。</li>
	<li style="padding:0 1em"><strong>5.4</strong> 与本项作业有关的其他记录与表格等。</li>
</ul>

<h2>6. 安全风险</h2>

<h3 style="padding:0 1em">6.1 风险名称：</h3>

<blockquote>
<p style="text-indent: 2em">人身伤害</p>
</blockquote>

<h3 style="padding:0 1em">6.2 防控措施：</h3>

<ul style="padding:0 1em">
	<li style="padding:0 1em"><strong>6.2.1</strong> 确认封锁后，方可上道作业。</li>
	<li style="padding:0 1em"><strong>6.2.2</strong> 在无人行道桥上禁止站在钢轨外面起钉；更换护木螺栓不得向桥外方向使劲，防止摔下。</li>
	<li style="padding:0 1em"><strong>6.2.3</strong> 抬运起落钢轨要动作协调。不得将手脚伸入轨底或枕底。</li>
	<li style="padding:0 1em"><strong>6.2.4</strong> 抽换桥枕时要戴手套、安全带；人要站稳，禁止站在被换枕木头上，防止摔下。</li>
</ul>
