# 附注

## 引用的文件

> 1. 《铁路技术管理规程》（普速铁路部分）铁总科技[2014]172号
2. 《铁路技术管理规程》（高速铁路部分）铁总科技[2014]172号
3. 《普速铁路工务安全规则》铁总运[2014]272号
4. 《高速铁路工务安全规则》铁总运[2014]170号
5. 《铁路桥隧建筑物修理规则》铁运[2010]38号文
6. 《高速铁路桥隧建筑物修理规则》（试行）铁运[2011]131号
7. 《铁路营业线施工安全管理办法》铁运[2012]280号
8. 《广铁集团铁路营业线施工安全管理实施细则》广铁运发[2012]310号
9. 《广铁集团铁路营业线施工安全管理补充细则》广铁运发[2015]279号
10. 《广铁集团工务系统安全风险过程控制管理办法》
11. 《广铁集团工务系统铁路营业线作业安全管理实施细则》
12. 《广铁集团工务系统普速铁路安全防护管理办法》
13. 《广铁集团工务系统劳动安全管理办法》
14. 《广铁集团普速铁路桥隧修理实施细则》
15. 《广铁集团高速铁路桥隧修理实施细则》

## 本指导书使用说明

> 1. 本指导书对桥隧工的主要工作内容和标准进行了描述，仅具有指导作用，未尽事宜请参考相关规章、文电。
2. 本书存放在桥隧车间及工区，供桥隧工翻阅学习。
3. 本书由广铁集团工务处负责解释、修订。
